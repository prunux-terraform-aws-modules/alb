### Security Group Rules for lb ###
# Allows all incoming traffic on Port 80 and 443 and all outgoing
resource "aws_security_group" "alb_security_group" {
  name        = "alb-sg-${var.name}"
  description = "ALB access rules"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "instance" {
  name            = "alb-${var.name}"
  internal        = "${var.internal}"
  security_groups = ["${aws_security_group.alb_security_group.id}"]
  subnets         = ["${var.subnets}"]
  idle_timeout    = "${var.idle_timeout}"
  ip_address_type = "${var.ip_address_type}"

  tags {
    Environment = "${var.env}"
    Application = "${var.application}"
  }
}

# HTTP
locals {
  has_http                 = "${var.https_only == "true" ? 0 : 1}"
  http_attachments_counter = "${local.has_http ? length(var.instances) : 0}"
}

resource "aws_lb_listener" "http_listener" {
  count             = "${local.has_http}"
  load_balancer_arn = "${aws_lb.instance.arn}"
  port              = "80"

  default_action {
    target_group_arn = "${aws_lb_target_group.http_target.arn}"
    type             = "forward"
  }
}

resource "aws_lb_target_group" "http_target" {
  count       = "${local.has_http}"
  name        = "alb-target-${var.name}"
  port        = "${var.target_port_http}"
  protocol    = "HTTP"
  vpc_id      = "${var.vpc_id}"
  target_type = "${var.target_type}"

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    timeout             = "${var.health_check_timeout}"
    path                = "${var.health_check_path}"
    interval            = "${var.health_check_interval}"
    matcher             = "${var.health_check_matcher}"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = "${var.stickiness_cookie_duration}"
    enabled         = "${var.stickiness_enabled}"
  }

  tags {
    Name = "HTTP alb-target-${var.name}"
  }
}

resource "aws_lb_target_group_attachment" "http_attachment" {
  count            = "${local.http_attachments_counter}"
  target_group_arn = "${aws_lb_target_group.http_target.arn}"
  target_id        = "${element(var.instances, count.index)}"
  port             = "${var.target_port_http}"
}

# HTTPS
locals {
  has_https                 = "${var.ssl_certificate_id == "none" ? 0 : 1}"
  https_attachments_counter = "${local.has_https ? length(var.instances) : 0}"
}

resource "aws_lb_listener" "https_listener" {
  count             = "${local.has_https}"
  load_balancer_arn = "${aws_lb.instance.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${var.ssl_certificate_id}"

  default_action {
    target_group_arn = "${aws_lb_target_group.https_target.arn}"
    type             = "forward"
  }
}

resource "aws_lb_target_group" "https_target" {
  count       = "${local.has_https}"
  name        = "alb-target-${var.name}"
  port        = "${var.target_port_https}"
  protocol    = "HTTPS"
  vpc_id      = "${var.vpc_id}"
  target_type = "${var.target_type}"

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    timeout             = "${var.health_check_timeout}"
    path                = "${var.health_check_path}"
    interval            = "${var.health_check_interval}"
    matcher             = "${var.health_check_matcher}"
    protocol            = "HTTPS"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = "${var.stickiness_cookie_duration}"
    enabled         = "${var.stickiness_enabled}"
  }

  tags {
    Name = "HTTPS alb-target-${var.name}"
  }
}

resource "aws_lb_target_group_attachment" "https_attachment" {
  count            = "${local.https_attachments_counter}"
  target_group_arn = "${aws_lb_target_group.https_target.arn}"
  target_id        = "${element(var.instances, count.index)}"
  port             = "${var.target_port_https}"
}
